
let Select2MultiCheckBoxObj = [];
let selectedBox  =[];
$(document).ready(function () {
    //in customer condition task , show first item and hide all anthor items 
    $(".customerOperations").children(".customerOption").hide()
    $(".customerOperations").children(".customerOption").eq(0).show()

    //===========================================================
    $.map($('#cities option'), function (option) {
        AddItemInSelect2MultiCheckBoxObj(option.value, false);
    });

   
    function formatResult(state) {
        console.log()
        if (Select2MultiCheckBoxObj.length > 0) {
            let stateId = 'option'+ state.id;
            let index = Select2MultiCheckBoxObj.findIndex(x => x.id == state.id);
            if (index > -1) {
                let checkbox = $('<div class="checkbox d-flex algin-items-center"><input class="select2Checkbox me-2" id="' + stateId + '" type="checkbox" ' + (Select2MultiCheckBoxObj[index]["IsChecked"] ? 'checked' : '') +
                    '><label for="' + stateId + '">' + state.text + '</label></div>');
                return checkbox;
            }
        }
    }

    
    let select2Options = {
        placeholder: "Select a city",
        allowClear: true,
        templateResult: formatResult,
        allowHtml: true,
        closeOnSelect : false,
    }
    
    let $select2 = $("#cities").select2(select2Options)
    $select2.on("select2:select", function (event) {
        buildSelectedContainer(true ,event)
        $("#option" + event.params.data.id).prop("checked", true);
        AddItemInSelect2MultiCheckBoxObj(event.params.data.id, true);
    });
    $select2.on("select2:unselect", function (event) {
        buildSelectedContainer(false ,event)
        $("#option" + event.params.data.id).prop("checked", false); 
        AddItemInSelect2MultiCheckBoxObj(event.params.data.id, false);
    });


    //detect customer select dropdown list change 
    $("#customer").change(function(event) {
        let index = event.target.selectedIndex
        $(".customerOperations").children(".customerOption").hide()
        $(".customerOperations").children(".customerOption").eq(index).show()
    })
    
})


function AddItemInSelect2MultiCheckBoxObj(id, IsChecked) {
    if (Select2MultiCheckBoxObj.length > 0) {
        let index = Select2MultiCheckBoxObj.findIndex(x => x.id == id);
        if (index > -1) {
            Select2MultiCheckBoxObj[index]["IsChecked"] = IsChecked;
        }
        else {
            Select2MultiCheckBoxObj.push({ "id": id, "IsChecked": IsChecked });
        }
    }
    else {
        Select2MultiCheckBoxObj.push({ "id": id, "IsChecked": IsChecked });
    }
}

function buildSelectedContainer(type ,item) {
    if(type) {
        selectedBox.push(item)
        $(".selectionBox").append('<div class="col-md-3">\
        <div class="single-option">\
            <button class="btn" data-itemID="'+item.params.data.id+'" id="close'+item.params.data.id+'" onclick="removeSelectedItem(this)">x</button><span>'+item.params.data.text+'</span>\
         </div>\
        </div>')
    }else {
        let id = item.params.data.id;
        let index = selectedBox.findIndex(x => x.params.data.id == id);
        selectedBox.splice(index , 1)
        console.log(selectedBox)
        console.log($(".selectionBox").children())
        $(".selectionBox").children('.col-md-3').eq(index).remove()
    }
   
}

function removeSelectedItem(event) {
    let id  = event.getAttribute('data-itemID')
    let closeButton =  $('#select2-cities-container li[title='+id+'] button')
    closeButton[0].click()
 
    $(event).closest('div.col-md-3').remove()
}